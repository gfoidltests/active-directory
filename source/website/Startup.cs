﻿using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using gfoidl.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace website
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        //---------------------------------------------------------------------
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(GetConfigPath(env))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }
        //---------------------------------------------------------------------
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.Configure<AuthSettings>(this.Configuration.GetSection("AuthSettings"));
        }
        //---------------------------------------------------------------------
        public void ConfigureProductionServices(IServiceCollection services)
        {
            // Needed for Loadbalancer and AntiforgeryTokens
            // http://www.tugberkugurlu.com/archive/asp-net-core-authentication-in-a-load-balanced-environment-with-haproxy-and-redis
            var redisHost      = this.Configuration.GetValue<string>("Redis");
            var redisIpAddress = Dns.GetHostEntryAsync(redisHost).Result.AddressList.Last();
            var redis          = ConnectionMultiplexer.Connect(redisIpAddress.ToString());
            services.AddDataProtection().PersistKeysToRedis(redis);

            this.ConfigureServices(services);
        }
        //---------------------------------------------------------------------
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            // https://nblumhardt.com/2016/10/aspnet-core-file-logger/
            loggerFactory.AddFile(this.Configuration.GetSection("Serilog"));

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseStaticFiles();
            }
            else
                app.UseExceptionHandler("/Home/Error");

            this.ConfigureAuth(app, env, loggerFactory);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        //---------------------------------------------------------------------
        private void ConfigureAuth(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var authSettings = this.Configuration.GetSection("AuthSettings").Get<AuthSettings>();
            JwtBearerAuthentication.ConfigureAuth(app, env, authSettings, loggerFactory);
        }
        //---------------------------------------------------------------------
        private static string GetConfigPath(IHostingEnvironment env)
        {
            if (env.IsProduction() && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return $"/etc/{env.ApplicationName}";
            else
                return Path.Combine(env.ContentRootPath, "config");
        }
    }
}