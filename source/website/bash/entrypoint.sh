#!/bin/bash

set -e

if [[ $# -lt 1 ]]; then
    echo login path must be given as first argument
    exit 1
fi

export AuthSettings__LoginPath="$1"
shift

Redis=$(getent hosts redis | awk '{ print $1 }')
export Redis

if [[ $# -eq 0 ]]; then
    exec dotnet website.dll
else
    exec "$@"
fi
