﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace website.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public IActionResult Index()
        {
            return this.View();
        }
        //---------------------------------------------------------------------
        [Authorize]
        public IActionResult Add()
        {
            return this.View();
        }
        //---------------------------------------------------------------------
        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Add(int a, int b)
        {
            this.ViewData["Result"] = a + b;
            return this.View();
        }
        //---------------------------------------------------------------------
        public IActionResult Error()
        {
            return this.View();
        }
    }
}