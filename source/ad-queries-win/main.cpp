#include <string>
#include <iostream>
#include <WinSock2.h>
#include "ldap.h"
#include "mechanism.h"
//-----------------------------------------------------------------------------
using namespace std;
//-----------------------------------------------------------------------------
int changePasswordTest();
int free();
//-----------------------------------------------------------------------------
LDAP* ld;
//-----------------------------------------------------------------------------
int main()
{
    char* ldapHost        = "ldap://10.0.0.16";
    char* ldapAdmin       = "Administrator@gfoidl.at";
    char* ldapPwd         = "Password1!";
    const int ldapVersion = LDAP_VERSION3;

    timeval timeOut = { 10, 0 };
    int rc;

    if ((rc = ldap_initialize(&ld, ldapHost)) != LDAP_SUCCESS)
    {
        cout << "LDAP session initialization failed: " << ldap_err2string(rc) << endl;
        free();
        return 1;
    }
    cout << "LDAP session initialized" << endl;

    if ((rc = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &ldapVersion)) != LDAP_SUCCESS)
    {
        cout << "Set option failed: " << ldap_err2string(rc) << endl;
        free();
        return 1;
    }
    cout << "Protocol version set" << endl;

    if ((rc = ldap_set_option(ld, LDAP_OPT_NETWORK_TIMEOUT, &timeOut)) != LDAP_SUCCESS)
    {
        cout << "Set option failed: " << ldap_err2string(rc) << endl;
        free();
        return 1;
    }
    cout << "Timeout set" << endl;

    berval creds;
    creds.bv_val = ldapPwd;
    creds.bv_len = strlen(ldapPwd);

    cout << endl;

    if ((rc = ldap_sasl_bind_s(ld, ldapAdmin, LDAP_SASL_SIMPLE, &creds, NULL, NULL, NULL)) != LDAP_SUCCESS)
    //if ((rc = ldap_gssapi_bind_s(ld, ldapAdmin, ldapPwd)) != LDAP_SUCCESS)        // not supported
    {
        cout << "Bind failed: " << ldap_err2string(rc) << endl;
        free();
        return 1;
    }
    cout << "Bind and authentification to server successful" << endl;

    changePasswordTest();

    return free();
}
//-----------------------------------------------------------------------------
int changePasswordTest()
{
    LDAPMod modUnicodePwd, *modify[2];
    char    *passValues[2];

    passValues[0] = "abc123#...";
    passValues[1] = NULL;

    modUnicodePwd.mod_op     = LDAP_MOD_REPLACE;
    modUnicodePwd.mod_type   = "unicodePwd";
    modUnicodePwd.mod_values = passValues;

    modify[0] = &modUnicodePwd;
    modify[1] = NULL;

    int rc = ldap_modify_ext_s(ld, "CN=himen,OU=Users,OU=test,DC=gfoidl,DC=at", modify, NULL, NULL);

    if (rc != LDAP_SUCCESS)
    {
        cout << "ldap_modify_ext_s: rc: " << rc << " " << ldap_err2string(rc) << endl;
        return 1;
    }

    cout << "password changed";

    return 0;
}
//-----------------------------------------------------------------------------
int free()
{
    cout << endl;
    cout << "cleaning up mememory..." << endl;
    
    int rc;
    if ((rc = ldap_unbind_ext(ld, NULL, NULL)) != LDAP_SUCCESS)
    {
        cout << "Unbind failed: " << ldap_err2string(rc) << endl;
        free();
        return 1;
    }

    cout << "memory cleaned." << endl;

    return 0;
}