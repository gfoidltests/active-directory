#pragma once

#define LDAP_MECHANISM_EXTERNAL     "EXTERNAL"
#define LDAP_MECHANISM_CRAMMD5      "CRAM-MD5"
#define LDAP_MECHANISM_GSSAPI       "GSSAPI"
//#define LDAP_SASL_SIMPLE          ""                  // already defined
#define LDAP_MECHANISM_DIGEST       "DIGEST-MD5"