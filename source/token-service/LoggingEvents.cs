﻿namespace token_service
{
    public static class LoggingEvents
    {
        public const int LogIn          = 1000;
        public const int RefreshToken   = 1001;
        public const int ChangePassword = 1002;
    }
}