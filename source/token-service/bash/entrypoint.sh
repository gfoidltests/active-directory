#!/bin/bash

set -e

if [[ $# -lt 1 ]]; then
    echo ldap-host must be set as first argument
    exit 1
fi

export ActiveDirectory__LdapHost=$1
shift

if [[ "$1" == "--dev" ]]; then
    export ASPNETCORE_ENVIRONMENT=Development
    shift
fi

if [[ $# -eq 0 ]]; then
    exec dotnet token-service.dll
else
    exec "$@"
fi
