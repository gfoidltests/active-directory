﻿namespace token_service
{
    public class AuthSettings
    {
        public string Issuer              { get; set; }
        public string Audience            { get; set; }
        public string CertKeyPath         { get; set; }
        public int ExpirationInMinutes    { get; set; }
        public string CookieName          { get; set; }
        public string ReturnUrlCookieName { get; set; }
    }
}