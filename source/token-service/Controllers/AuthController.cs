﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using gfoidl.ActiveDirectory;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using token_service.Contracts;
using token_service.Models;

namespace token_service.Controllers
{
    public class AuthController : Controller
    {
        private readonly IActiveDirectory    _activeDirectory;
        private readonly ITokenService       _tokenService;
        private readonly AuthSettings        _authSettings;
        private readonly ILogger             _logger;
        private readonly IHostingEnvironment _env;
        //---------------------------------------------------------------------
        public AuthController(
            IActiveDirectory        activeDirectory,
            ITokenService           tokenService,
            IOptions<AuthSettings>  authSettings,
            ILogger<AuthController> logger,
            IHostingEnvironment env)
        {
            _activeDirectory = activeDirectory     ?? throw new ArgumentNullException(nameof(activeDirectory));
            _tokenService    = tokenService        ?? throw new ArgumentNullException(nameof(tokenService));
            _authSettings    = authSettings?.Value ?? throw new ArgumentNullException(nameof(authSettings));
            _logger          = logger              ?? throw new ArgumentNullException(nameof(logger));
            _env             = env                 ?? throw new ArgumentNullException(nameof(env));
        }
        //---------------------------------------------------------------------
        [HttpGet]
        public ViewResult Login()
        {
            return this.View();
        }
        //---------------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string userName, string password)
        {
            try
            {
                var res = await this.LoginCore(userName, password);

                if (!res.AdUser.IsUserValid && res.AdUser.DisplayName != null)
                    return this.RedirectToAction("UserNotValid", new { userName });

                if (!res.AdUser.Authenticated)
                {
                    this.ViewData["Message"] = "Username or password are incorrect";
                    return this.View("Login");
                }

                this.SetAccessTokenCookie(res.Token);

                string returnUrl = this.Request.Cookies[_authSettings.ReturnUrlCookieName];

                if (string.IsNullOrWhiteSpace(returnUrl))
                    return this.View();

                this.DeleteReturnUrlCookie();
                return this.Redirect(returnUrl);
            }
            catch (ArgumentNullException)
            {
                return this.BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.LogIn, ex, ex.Message);
                throw;
            }
        }
        //---------------------------------------------------------------------
        [HttpGet]
        public ViewResult ChangePassword()
        {
            return this.View();
        }
        //---------------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            if (!this.ModelState.IsValid) return this.View();

            try
            {
                if (!_activeDirectory.ChangePassword(changePasswordViewModel.UserName, changePasswordViewModel.OldPassword, changePasswordViewModel.NewPassword))
                {
                    this.ViewData["Success"] = false;
                    this.ViewData["Message"] = "User does not exist or wrong credentials";
                    return this.View();
                }

                _logger.LogInformation(LoggingEvents.ChangePassword, "Password changed for user {u}", changePasswordViewModel.UserName);

                this.ViewData["Success"] = true;
                this.ViewData["Message"] = "Password is changed.";
                return this.View();
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.ChangePassword, ex, ex.Message);
                throw;
            }
        }
        //---------------------------------------------------------------------
        [HttpPost("api/token")]
        public async Task<IActionResult> Token(string userName, string password)
        {
            try
            {
                var res = await this.LoginCore(userName, password);

                if (!res.AdUser.Authenticated) return this.Unauthorized();

                return this.Ok(res.Token);
            }
            catch (Exception)
            {
                return this.BadRequest();
            }
        }
        //---------------------------------------------------------------------
        [HttpPost("api/refresh")]
        public IActionResult RefreshToken(string token, bool setCookie = false)
        {
            try
            {
                ClaimsPrincipal principal = _tokenService.CreateClaimsPrincipal(token);
                JwtSecurityToken jwtToken = _tokenService.CreateJwtSecurityToken(principal, false);
                string serialiazedToken   = _tokenService.SerialiazeToken(jwtToken);

                if (setCookie)
                    this.SetAccessTokenCookie(serialiazedToken);

                _logger.LogInformation(LoggingEvents.RefreshToken, "token refreshed for {u}", principal?.Identity?.Name);

                return this.Ok(serialiazedToken);
            }
            catch (Exception)
            {
                return this.BadRequest();
            }
        }
        //---------------------------------------------------------------------
        [HttpGet]
        public ViewResult UserNotValid(string userName)
        {
            this.ViewData["UserName"] = userName;

            return this.View();
        }
        //---------------------------------------------------------------------
        private async Task<(string Token, AdUser AdUser)> LoginCore(string userName, string password)
        {
            var res = await this.LoginCoreImpl(userName, password);

            if (res.AdUser.Authenticated)
                _logger.LogInformation(LoggingEvents.LogIn, "{u} logged in", userName);
            else
                _logger.LogInformation(LoggingEvents.LogIn, "{u} not logged in", userName);

            return res;
        }
        //---------------------------------------------------------------------
        private async Task<(string Token, AdUser AdUser)> LoginCoreImpl(string userName, string password)
        {
            if (string.IsNullOrWhiteSpace(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException(nameof(password));

            // Mitigate brute force
            if (_env.IsProduction()) await Task.Delay(250);

            AdUser adUser = _activeDirectory.Login(userName, password);

            if (!adUser.IsUserValid || !adUser.Authenticated)
                return (null as string, adUser);

            ClaimsPrincipal principal = _tokenService.CreateClaimsPrincipal(adUser);
            JwtSecurityToken token    = _tokenService.CreateJwtSecurityToken(principal);
            string serialiazedToken   = _tokenService.SerialiazeToken(token);

            return (serialiazedToken, adUser);
        }
        //---------------------------------------------------------------------
        private void SetAccessTokenCookie(string token)
        {
            this.Response.Cookies.Append(_authSettings.CookieName, token, GetCookieOptions());
            //-----------------------------------------------------------------
            CookieOptions GetCookieOptions()
            {
                var cookieOptions = new CookieOptions
                {
                    HttpOnly = true,
                    Expires  = DateTime.UtcNow.AddMinutes(_authSettings.ExpirationInMinutes),
                };

                if (!_env.IsDevelopment())
                    cookieOptions.Domain = GetCookieDomain();

                return cookieOptions;
            }
            //-----------------------------------------------------------------
            string GetCookieDomain() => new Uri(_authSettings.Audience).Host;
        }
        //---------------------------------------------------------------------
        private void DeleteReturnUrlCookie()
        {
            this.Response.Cookies.Delete(_authSettings.ReturnUrlCookieName, new CookieOptions
            {
                Domain = _authSettings.Issuer.GetCookieDomain()
            });
        }
    }
}