﻿using System.IO;
using System.Runtime.InteropServices;
using gfoidl.ActiveDirectory;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using token_service.Contracts;
using token_service.Services;

namespace token_service
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        //---------------------------------------------------------------------
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(GetConfigPath(env))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }
        //---------------------------------------------------------------------
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.Configure<AuthSettings>(this.Configuration.GetSection("AuthSettings"));
            services.Configure<AdSettings>(this.Configuration.GetSection("ActiveDirectory"));

            services.AddTransient<IActiveDirectory, LazyActiveDirectory>();
            services.AddTransient<ITokenService   , TokenService>();
        }
        //---------------------------------------------------------------------
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            // https://nblumhardt.com/2016/10/aspnet-core-file-logger/
            loggerFactory.AddFile(this.Configuration.GetSection("Serilog"));

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
                app.UseExceptionHandler("/Home/Error");

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Auth}/{action=Login}/{id?}");
            });
        }
        //---------------------------------------------------------------------
        private static string GetConfigPath(IHostingEnvironment env)
        {
            if (env.IsProduction() && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return $"/etc/{env.ApplicationName}";
            else
                return Path.Combine(env.ContentRootPath, "config");
        }
    }
}