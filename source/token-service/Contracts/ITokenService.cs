﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using gfoidl.ActiveDirectory;

namespace token_service.Contracts
{
    public interface ITokenService
    {
        ClaimsPrincipal CreateClaimsPrincipal(AdUser adUser);
        ClaimsPrincipal CreateClaimsPrincipal(string jwtToken);
        JwtSecurityToken CreateJwtSecurityToken(ClaimsPrincipal principal, bool addTokenClaims = true);
        string SerialiazeToken(JwtSecurityToken token);
    }
}