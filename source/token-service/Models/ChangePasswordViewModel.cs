﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace token_service.Models
{
    public class ChangePasswordViewModel : IValidatableObject
    {
        [Required, DisplayName("Username")]
        public string UserName             { get; set; }
        [Required, DisplayName("Current Password")]
        public string OldPassword          { get; set; }
        [Required, DisplayName("New Password")]
        public string NewPassword          { get; set; }
        [Required, DisplayName("Password confirmation"), Compare(nameof(NewPassword))]
        public string ConfirmedNewPassword { get; set; }
        //---------------------------------------------------------------------
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.NewPassword != this.ConfirmedNewPassword)
                yield return new ValidationResult(
                    "The new password and the confirmed password must be equal.",
                    new string[] { nameof(this.NewPassword), nameof(this.ConfirmedNewPassword) });
        }
    }
}