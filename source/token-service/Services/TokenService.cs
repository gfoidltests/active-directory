﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using gfoidl.ActiveDirectory;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using token_service.Contracts;

namespace token_service.Services
{
    public class TokenService : ITokenService
    {
        private readonly AuthSettings _authSettings;
        private const string Algorithm = SecurityAlgorithms.RsaSha256;
        //---------------------------------------------------------------------
        public TokenService(IOptions<AuthSettings> authSettings)
        {
            _authSettings = authSettings?.Value ?? throw new ArgumentNullException(nameof(authSettings));
        }
        //---------------------------------------------------------------------
        public ClaimsPrincipal CreateClaimsPrincipal(AdUser adUser)
        {
            var userIdentity = this.CreateClaimsIdentity(adUser);

            return new ClaimsPrincipal(userIdentity);
        }
        //---------------------------------------------------------------------
        public ClaimsPrincipal CreateClaimsPrincipal(string jwtToken)
        {
            var signgingCredentials = this.GetSigngingCredentials();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey         = signgingCredentials.Key,

                ValidateIssuer = true,
                ValidIssuer    = _authSettings.Issuer,

                ValidateAudience = true,
                ValidAudience    = _authSettings.Audience,

                ValidateLifetime = true,
                ClockSkew        = TimeSpan.FromSeconds(5),

                RequireSignedTokens   = true,
                RequireExpirationTime = true
            };

            var handler               = new JwtSecurityTokenHandler();
            ClaimsPrincipal principal = handler.ValidateToken(jwtToken, tokenValidationParameters, out SecurityToken validatedToken);
            var validJwt              = validatedToken as JwtSecurityToken;

            if (validJwt == null)                 throw new ArgumentException("Invalid JWT");
            if (validJwt.Header.Alg != Algorithm) throw new ArgumentException($"Algorithm must be '{Algorithm}'");

            return principal;
        }
        //---------------------------------------------------------------------
        public JwtSecurityToken CreateJwtSecurityToken(ClaimsPrincipal principal, bool addTokenClaims = true)
        {
            string userName                       = principal.FindFirst(ClaimTypes.Name).Value;
            SigningCredentials signingCredentials = this.GetSigngingCredentials().Credentials;
            IEnumerable<Claim> claims             = principal.Claims;

            if (addTokenClaims)
                claims = claims.Union(GetTokenClaims(userName));

            return new JwtSecurityToken(
                addTokenClaims ? _authSettings.Issuer : null,
                addTokenClaims ? _authSettings.Audience : null,
                claims,
                DateTime.UtcNow,
                DateTime.UtcNow.AddMinutes(_authSettings.ExpirationInMinutes),
                signingCredentials);
        }
        //---------------------------------------------------------------------
        public string SerialiazeToken(JwtSecurityToken token) => new JwtSecurityTokenHandler().WriteToken(token);
        //---------------------------------------------------------------------
        private (SigningCredentials Credentials, SecurityKey Key) GetSigngingCredentials()
        {
            // Although the X509Certificate implements IDisposabe don't dispose, because
            // otherwise the token can't be serialized
            var certificate = new X509Certificate2(_authSettings.CertKeyPath);
            var signingKey  = new X509SecurityKey(certificate);
            var credentials = new SigningCredentials(signingKey, Algorithm);

            return (credentials, signingKey);
        }
        //---------------------------------------------------------------------
        private ClaimsIdentity CreateClaimsIdentity(AdUser adUser)
        {
            string issuer = _authSettings.Issuer;

            var userIdentity = new ClaimsIdentity("Active Diretory");
            userIdentity.AddClaim(new Claim(ClaimTypes.Name, adUser.UserName   , ClaimValueTypes.String, issuer));
            userIdentity.AddClaim(new Claim("DisplayName"  , adUser.DisplayName, ClaimValueTypes.String, issuer));

            foreach (string group in adUser.Groups)
                userIdentity.AddClaim(new Claim(group, "true", ClaimValueTypes.String, issuer));

            return userIdentity;
        }
        //---------------------------------------------------------------------
        private static IEnumerable<Claim> GetTokenClaims(string userName)
        {
            // Nonce
            yield return new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());

            // Issued timestamp
            yield return new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.Now).ToString(), ClaimValueTypes.Integer64);

            // Subject / User
            yield return new Claim(JwtRegisteredClaimNames.Sub, userName);
        }
        //---------------------------------------------------------------------
        private static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}