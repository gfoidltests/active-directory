﻿using System;
using gfoidl.ActiveDirectory;
using Microsoft.Extensions.Options;

namespace token_service.Services
{
    public class LazyActiveDirectory : IActiveDirectory
    {
        private readonly Lazy<ActiveDirectory> _activeDirectory;
        //---------------------------------------------------------------------
        public LazyActiveDirectory(IOptions<AdSettings> adSettings)
        {
            _activeDirectory = new Lazy<ActiveDirectory>(CreateActiveDirectory);
            //-----------------------------------------------------------------
            ActiveDirectory CreateActiveDirectory()
            {
                return new ActiveDirectory(adSettings.Value);
            }
        }
        //---------------------------------------------------------------------
        public AdUser Login                (string userName, string password)                        => _activeDirectory.Value.Login(userName, password);
        public string GetDisplayNameForUser(string userName)                                         => _activeDirectory.Value.GetDisplayNameForUser(userName);
        public string GetValue             (string userName, string attributeName)                   => _activeDirectory.Value.GetValue(userName, attributeName);
        public bool VerifyPassword         (string userName, string password)                        => _activeDirectory.Value.VerifyPassword(userName, password);
        public bool IsUserValid            (string userName)                                         => _activeDirectory.Value.IsUserValid(userName);
        public bool ChangePassword         (string userName, string oldPassword, string newPassword) => _activeDirectory.Value.ChangePassword(userName, oldPassword, newPassword);
        //---------------------------------------------------------------------
        public void Dispose()
        {
            if (_activeDirectory.IsValueCreated) _activeDirectory.Value.Dispose();
        }
    }
}