﻿class LoginViewModel {
    UserName : KnockoutObservable<string>;
    Password : KnockoutObservable<string>;
    IsEnabled: KnockoutObservable<boolean>;

    constructor() {
        this.UserName = ko.observable("");
        this.Password = ko.observable("");

        this.IsEnabled = ko.computed(() => {
            return this.UserName().length > 0 && this.Password().length > 0;
        });
    }
}

$(() => {
    ko.applyBindings(new LoginViewModel());

    $(".my-tooltip").tooltipster({
        theme: "tooltipster-light",
        delay: 250,
        position: "right"
    });
});