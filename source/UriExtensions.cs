﻿using System.Text.RegularExpressions;

namespace System
{
    public static class UriExtensions
    {
        public static string GetCookieDomain(this string url)
        {
            var match = Regex.Match(url, @"\.\w+\.\w+$", RegexOptions.IgnoreCase);

            if (!match.Success) return url;

            return match.Value;
        }
    }
}