﻿using System.Collections.Generic;

namespace gfoidl.ActiveDirectory
{
    public class AdUser
    {
        public string UserName      { get; internal set; }
        public string DisplayName   { get; internal set; }
        public bool IsUserValid     { get; internal set; }
        public bool Authenticated   { get; internal set; }
        public IList<string> Groups { get; internal set; }
    }
}