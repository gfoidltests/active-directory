﻿using System;
using System.Linq;

namespace gfoidl.ActiveDirectory
{
    public class ActiveDirectory : DisposableObject, IActiveDirectory
    {
        private readonly ILdapAdapter _ldapAdapter;
        //---------------------------------------------------------------------
        public ActiveDirectory(AdSettings adSettings = null, ILdapAdapter ldapAdapter = null)
        {
            if (adSettings == null && ldapAdapter == null)
                throw new ArgumentNullException($"Either {nameof(adSettings)} od {nameof(ldapAdapter)} must be given.");

            _ldapAdapter = ldapAdapter ?? new LdapAdapter(adSettings);
        }
        //---------------------------------------------------------------------
        public AdUser Login(string userName, string password)
        {
            AdUser adUser      = new AdUser { UserName = userName };
            adUser.DisplayName = this.GetDisplayNameForUser(userName);
            adUser.IsUserValid = this.IsUserValid(userName);

            if (!adUser.IsUserValid) return adUser;

            adUser.Authenticated = this.VerifyPassword(userName, password);

            if (!adUser.Authenticated) return adUser;

            adUser.Groups      = _ldapAdapter.GetGroupsForUser(userName).ToList();
#if DEBUG
            adUser.DisplayName += " DEBUG";
#endif
            return adUser;
        }
        //---------------------------------------------------------------------
        public bool VerifyPassword(string userName, string password)
        {
            try
            {
                _ldapAdapter.Bind(userName, password);
                return true;
            }
#pragma warning disable 0168
            catch (Exception ex)        // for debugging
#pragma warning restore 0168
            {
                return false;
            }
        }
        //---------------------------------------------------------------------
        public bool IsUserValid(string userName)
        {
            try
            {
                string userAccountControl = _ldapAdapter.GetValue(userName, "userAccountControl");
                int value                 = int.Parse(userAccountControl);

                // http://www.selfadsi.de/ads-attributes/user-userAccountControl.htm
                return (value & (1 << 1)) == 0;
            }
            catch
            {
                return false;
            }
        }
        //---------------------------------------------------------------------
        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (!this.VerifyPassword(userName, oldPassword)) return false;

            return _ldapAdapter.ChangePassword(userName, newPassword);
        }
        //---------------------------------------------------------------------
        public string GetDisplayNameForUser(string userName)                       => _ldapAdapter.GetValue(userName, "displayName");
        public string GetValue             (string userName, string attributeName) => _ldapAdapter.GetValue(userName, attributeName);
        //---------------------------------------------------------------------
        protected override void DisposeCore() => _ldapAdapter?.Dispose();
    }
}