﻿using System;

namespace gfoidl.ActiveDirectory
{
    public abstract class DisposableObject : IDisposable
    {
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        protected void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                    this.DisposeCore();

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        protected abstract void DisposeCore();
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}