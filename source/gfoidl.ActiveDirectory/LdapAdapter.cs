﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Novell.Directory.Ldap;

/*
 * Note:
 * 
 * When 
 * System.Security.Authentication.AuthenticationException: 
 * 'The remote certificate is invalid according to the validation procedure.'
 * is thrown, the server certificate must be added to the trusted certifactes
 * on the machine running this code.
 * 
 * Here the server certificates reside in ./samba-data/var/lib/samba/private/tls
 */

namespace gfoidl.ActiveDirectory
{
    internal class LdapAdapter : DisposableObject, ILdapAdapter
    {
        private readonly AdSettings     _adSettings;
        private readonly LdapConnection _ldapConnection;
        //---------------------------------------------------------------------
        public LdapAdapter(AdSettings adSettings)
        {
            _adSettings     = adSettings ?? throw new ArgumentNullException(nameof(adSettings));
            _ldapConnection = new LdapConnection();

            _ldapConnection.SecureSocketLayer = adSettings.UseSsl;
            _ldapConnection.Connect(adSettings.LdapHost, adSettings.UseSsl ? LdapConnection.DEFAULT_SSL_PORT : LdapConnection.DEFAULT_PORT);
        }
        //---------------------------------------------------------------------
        public void Bind(string userName, string password) => _ldapConnection.Bind(LdapConnection.Ldap_V3, userName, password);
        //---------------------------------------------------------------------
        public string GetValue(string userName, string attributeName)
        {
            _ldapConnection.Bind(LdapConnection.Ldap_V3, _adSettings.Admin, _adSettings.Password);

            userName = GetUserNameWithoutDomain(userName);

            LdapSearchQueue searchQueue = _ldapConnection.Search(
                _adSettings.SearchBase,
                LdapConnection.SCOPE_SUB,
                $"(sAMAccountName={userName})",
                new string[] { attributeName },
                false,
                null as LdapSearchQueue);

            LdapMessage message;
            while ((message = searchQueue.getResponse()) != null)
            {
                if (message is LdapSearchResult searchResult)
                {
                    LdapEntry entry    = searchResult?.Entry;
                    LdapAttribute attr = entry?.getAttribute(attributeName);
                    return attr?.StringValue;
                }
                else
                    continue;
            }

            return null;
        }
        //---------------------------------------------------------------------
        public IEnumerable<string> GetGroupsForUser(string userName)
        {
            _ldapConnection.Bind(LdapConnection.Ldap_V3, _adSettings.Admin, _adSettings.Password);

            var groups       = new Stack<string>();
            var uniqueGroups = new HashSet<string>();
            userName         = GetUserNameWithoutDomain(userName);

            foreach (string group in this.GetGroupsForUserCore(userName))
                groups.Push(group);

            while (groups.Count > 0)
            {
                string group = groups.Pop();
                if (uniqueGroups.Add(group))
                    yield return group;

                foreach (string parentGroup in this.GetGroupsForUserCore(group))
                    groups.Push(parentGroup);
            }
        }
        //---------------------------------------------------------------------
        // http://www.dirmgr.com/blog/2010/8/26/ldap-password-changes-in-active-directory.html
        public bool ChangePassword(string userName, string newPassword)
        {
            string dn = this.GetValue(userName, "distinguishedName");

            if (dn == null)
                throw new InvalidOperationException($"User '{userName}' does not exists");

            const string passwordAttributeName = "unicodePwd";
            //const string passwordAttributeName = "userPassword";      // wrong

            byte[] value    = Encoding.Unicode.GetBytes("\"" + newPassword + "\"");
            newPassword     = Convert.ToBase64String(value);
            var addPassword = new LdapAttribute(passwordAttributeName);

            addPassword.addBase64Value(newPassword);

            var modification = new LdapModification(LdapModification.REPLACE, addPassword);

            _ldapConnection.Modify(dn, modification);

            return true;
        }
        //---------------------------------------------------------------------
        private IEnumerable<string> GetGroupsForUserCore(string user)
        {
            LdapSearchQueue searchQueue = _ldapConnection.Search(
                _adSettings.SearchBase,
                LdapConnection.SCOPE_SUB,
                $"(sAMAccountName={user})",
                new string[] { "cn", "memberOf" },
                false,
                null as LdapSearchQueue);

            LdapMessage message;
            while ((message = searchQueue.getResponse()) != null)
            {
                if (message is LdapSearchResult searchResult)
                {
                    LdapEntry entry = searchResult.Entry;
                    foreach (string value in HandleEntry(entry))
                        yield return value;
                }
                else
                    continue;
            }
            //-----------------------------------------------------------------
            IEnumerable<string> HandleEntry(LdapEntry entry)
            {
                LdapAttribute attr = entry.getAttribute("memberOf");

                if (attr == null) yield break;

                foreach (string value in attr.StringValueArray)
                {
                    string groupName = GetGroup(value);
                    yield return groupName;
                }
            }
            //-----------------------------------------------------------------
            string GetGroup(string value)
            {
                Match match = Regex.Match(value, "^CN=([^,]*)");

                if (!match.Success) return null;

                return match.Groups[1].Value;
            }
        }
        //---------------------------------------------------------------------
        private static string GetUserNameWithoutDomain(string userName) => userName.Split('@')[0];
        //---------------------------------------------------------------------
        protected override void DisposeCore() => _ldapConnection?.Dispose();
    }
}