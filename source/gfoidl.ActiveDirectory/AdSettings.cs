﻿namespace gfoidl.ActiveDirectory
{
    public class AdSettings
    {
        public string LdapHost   { get; set; }
        public bool UseSsl       { get; set; } = true;
        public string Admin      { get; set; }
        public string Password   { get; set; }
        public string SearchBase { get; set; }
    }
}