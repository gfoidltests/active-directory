﻿using System;

namespace gfoidl.ActiveDirectory
{
    public interface IActiveDirectory : IDisposable
    {
        AdUser Login                (string userName, string password);
        string GetDisplayNameForUser(string userName);
        string GetValue             (string userName, string attributeName);
        bool VerifyPassword         (string userName, string password);
        bool IsUserValid            (string userName);
        bool ChangePassword         (string userName, string oldPassword, string newPassword);
    }
}