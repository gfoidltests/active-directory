﻿using System;
using System.Collections.Generic;

namespace gfoidl.ActiveDirectory
{
    public interface ILdapAdapter : IDisposable
    {
        void Bind                           (string userName, string password);
        string GetValue                     (string userName, string attributeName);
        IEnumerable<string> GetGroupsForUser(string userName);
        bool ChangePassword                 (string userName, string newPassword);
    }
}