﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using gfoidl.AspNetCore.Authentication.JwtBearer.Middleware;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace gfoidl.AspNetCore.Authentication.JwtBearer
{
    public static class JwtBearerAuthentication
    {
        public static void ConfigureAuth(IApplicationBuilder app, IHostingEnvironment env, AuthSettings authSettings, ILoggerFactory loggerFactory)
        {
            var signingKey = GetSecurityKey();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey         = signingKey,

                ValidateIssuer = true,
                ValidIssuer    = authSettings.Issuer,

                ValidateAudience = true,
                ValidAudience    = authSettings.Audience,

                ValidateLifetime = true,
                ClockSkew        = TimeSpan.FromSeconds(5),

                RequireSignedTokens   = true,
                RequireExpirationTime = true
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate     = true,
                AutomaticChallenge        = true,
                TokenValidationParameters = tokenValidationParameters
            });

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge    = true,
                AuthenticationScheme  = "Cookie",
                CookieName            = authSettings.CookieName,
                CookieHttpOnly        = true,
                ExpireTimeSpan        = TimeSpan.FromMinutes(authSettings.ExpirationInMinutes),
                TicketDataFormat      = new CustomJwtDataFormat(SecurityAlgorithms.RsaSha256, tokenValidationParameters, loggerFactory.CreateLogger<CustomJwtDataFormat>()),
                Events                = new CookieAuthenticationEvents
                {
                    OnRedirectToLogin = RedirectToLogin
                }
            });

            if (authSettings.RefreshToken)
                app.UseTokenRefresh();
            //-----------------------------------------------------------------
            //Func<CookieRedirectContext, Task>
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task RedirectToLogin(CookieRedirectContext context)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
            {
                HttpRequest request = context.HttpContext.Request;
                string requestUrl   = $"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}";
                string location     = authSettings.LoginPath;

                context.Response.Cookies.Append(authSettings.ReturnUrlCookieName, requestUrl, GetCookieOptions());
                context.HttpContext.Response.Redirect(location);
            }
            //---------------------------------------------------------------------
            CookieOptions GetCookieOptions()
            {
                var co = new CookieOptions();

                if (!env.IsDevelopment()) co.Domain = authSettings.Issuer.GetCookieDomain();

                return co;
            }
            //-----------------------------------------------------------------
            SecurityKey GetSecurityKey()
            {
                var certificate = new X509Certificate2(authSettings.CertPath);
                return new X509SecurityKey(certificate);
            }
        }
    }
}