﻿namespace gfoidl.AspNetCore.Authentication.JwtBearer
{
    public class AuthSettings
    {
        public string Issuer                   { get; set; }
        public string Audience                 { get; set; }
        public string CertPath                 { get; set; }
        public int ExpirationInMinutes         { get; set; }
        public string CookieName               { get; set; }
        public string ReturnUrlCookieName      { get; set; }
        public string LoginPath                { get; set; }
        public bool SlidingExpiration          { get; set; } = true;
        public bool RefreshToken               { get; set; } = true;
        public bool RefreshTokenOnEveryRequest { get; set; }
    }
}