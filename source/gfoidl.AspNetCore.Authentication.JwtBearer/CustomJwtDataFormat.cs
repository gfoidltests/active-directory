﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace gfoidl.AspNetCore.Authentication.JwtBearer
{
    public class CustomJwtDataFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string                    _algorithm;
        private readonly TokenValidationParameters _validationParameters;
        private readonly ILogger                   _logger;
        //---------------------------------------------------------------------
        public CustomJwtDataFormat(
            string algorithm,
            TokenValidationParameters validationParameters,
            ILogger<CustomJwtDataFormat> logger)
        {
            _algorithm            = algorithm            ?? throw new ArgumentNullException(nameof(algorithm));
            _validationParameters = validationParameters ?? throw new ArgumentNullException(nameof(validationParameters));
            _logger               = logger               ?? throw new ArgumentNullException(nameof(logger));
        }
        //---------------------------------------------------------------------
        public string Protect(AuthenticationTicket data)                 => throw new NotImplementedException();
        public string Protect(AuthenticationTicket data, string purpose) => throw new NotImplementedException();
        public AuthenticationTicket Unprotect(string protectedText)      => this.Unprotect(protectedText, null);
        //---------------------------------------------------------------------
        public AuthenticationTicket Unprotect(string protectedText, string purpose)
        {
            try
            {
                return this.UnproctectCore(protectedText);
            }
            catch (SecurityTokenValidationException ex)
            {
                _logger.LogError(new EventId(42, "Unprotect"), ex, ex.Message);
                return null;
            }
            catch (ArgumentException ex)
            {
                _logger.LogError(new EventId(42, "Unprotect"), ex, ex.Message);
                return null;
            }
        }
        //---------------------------------------------------------------------
        private AuthenticationTicket UnproctectCore(string protectedText)
        {
            var handler               = new JwtSecurityTokenHandler();
            ClaimsPrincipal principal = handler.ValidateToken(protectedText, _validationParameters, out SecurityToken validatedToken);
            var validJwt              = validatedToken as JwtSecurityToken;

            if (validJwt == null)                  throw new ArgumentException("Invalid JWT");
            if (validJwt.Header.Alg != _algorithm) throw new ArgumentException($"Algorithm must be '{_algorithm}'");

            var authenticationProperties = new AuthenticationProperties
            {
                ExpiresUtc   = validJwt.ValidTo,
                IssuedUtc    = validJwt.ValidFrom,
                IsPersistent = true,
                AllowRefresh = false
            };

            return new AuthenticationTicket(principal, authenticationProperties, "Cookie");
        }
    }
}