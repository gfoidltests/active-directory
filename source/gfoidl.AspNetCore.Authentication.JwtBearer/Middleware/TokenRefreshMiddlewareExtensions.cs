﻿using Microsoft.AspNetCore.Builder;

namespace gfoidl.AspNetCore.Authentication.JwtBearer.Middleware
{
    public static class TokenRefreshMiddlewareExtensions
    {
        public static IApplicationBuilder UseTokenRefresh(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TokenRefreshMiddleware>();
        }
    }
}