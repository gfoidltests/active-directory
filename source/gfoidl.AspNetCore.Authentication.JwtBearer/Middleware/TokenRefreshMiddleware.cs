﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace gfoidl.AspNetCore.Authentication.JwtBearer.Middleware
{
    public class TokenRefreshMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly HttpClient      _httpClient;
        private readonly ILogger         _logger;
        private readonly AuthSettings    _authSettings;
        private readonly bool            _isDevelopment;
        //---------------------------------------------------------------------
        public TokenRefreshMiddleware(
            RequestDelegate next,
            IOptions<AuthSettings> authSettings,
            ILoggerFactory loggerFactory,
            IHostingEnvironment hostingEnvironment)
        {
            _next          = next ?? throw new ArgumentNullException(nameof(next));
            _httpClient    = new HttpClient();
            _authSettings  = authSettings?.Value ?? throw new ArgumentNullException(nameof(authSettings));
            _logger        = loggerFactory?.CreateLogger<TokenRefreshMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
            _isDevelopment = hostingEnvironment?.IsDevelopment() ?? false;
        }
        //---------------------------------------------------------------------
        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Cookies.TryGetValue(_authSettings.CookieName, out string cookie))
                await this.HandelTokenRefreshAsync(context, cookie);

            await _next(context);
        }
        //---------------------------------------------------------------------
        private async Task HandelTokenRefreshAsync(HttpContext context, string oldToken)
        {
            if (!_authSettings.SlidingExpiration || string.IsNullOrWhiteSpace(oldToken)) return;

            AuthenticateInfo authenticationProperties = await context.Authentication.GetAuthenticateInfoAsync("Cookie");

            // When the header, etc. is manipulated
            if (authenticationProperties.Principal == null) return;

            var currentUtc = DateTimeOffset.UtcNow;
            var issuedUtc  = authenticationProperties.Properties.IssuedUtc;
            var expiresUtc = authenticationProperties.Properties.ExpiresUtc;

            var timeElapsed   = currentUtc.Subtract(issuedUtc.Value);
            var timeRemaining = expiresUtc.Value.Subtract(currentUtc);

            if (timeRemaining < timeElapsed || _authSettings.RefreshTokenOnEveryRequest)
                await this.RefreshTokenAsync(context, oldToken);
        }
        //---------------------------------------------------------------------
        private async Task RefreshTokenAsync(HttpContext context, string oldToken)
        {
            string refreshUrl = $"{_authSettings.LoginPath}/api/refresh";

            var requestContent = new Dictionary<string, string>
            {
                ["token"] = oldToken
            };

            var response    = await _httpClient.PostAsync(refreshUrl, new FormUrlEncodedContent(requestContent));
            response.EnsureSuccessStatusCode();
            string newToken = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrWhiteSpace(newToken))
                _logger.LogError("Token-refresh returned an empty token");

            this.SetCookie(context, newToken);

            if (_isDevelopment)
                context.Response.Headers.Add("X-Token-Refresh", "refreshed");
        }
        //---------------------------------------------------------------------
        private void SetCookie(HttpContext context, string token)
        {
            context.Response.Cookies.Append(_authSettings.CookieName, token, GetCookieOptions());
            //-----------------------------------------------------------------
            CookieOptions GetCookieOptions()
            {
                var cookieOptions = new CookieOptions
                {
                    HttpOnly = true,
                    Expires  = DateTime.UtcNow.AddMinutes(_authSettings.ExpirationInMinutes),
                };

                if (!_isDevelopment)
                    cookieOptions.Domain = GetCookieDomain();

                return cookieOptions;
            }
            //-----------------------------------------------------------------
            string GetCookieDomain() => new Uri(_authSettings.Audience).Host;
        }
    }
}