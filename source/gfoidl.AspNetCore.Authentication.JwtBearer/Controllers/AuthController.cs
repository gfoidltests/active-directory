﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace gfoidl.AspNetCore.Authentication.JwtBearer.Controllers
{
    public class AuthController : Controller
    {
        private readonly AuthSettings _authSettings;
        //---------------------------------------------------------------------
        public AuthController(IOptions<AuthSettings> authSettings)
        {
            _authSettings = authSettings?.Value ?? throw new ArgumentNullException(nameof(authSettings));
        }
        //---------------------------------------------------------------------
        [Authorize]
        public IActionResult Logout()
        {
            this.Response.Cookies.Delete(_authSettings.CookieName);
            return this.RedirectToAction("Index", "Home");
        }
    }
}