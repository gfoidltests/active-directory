#!/bin/bash

set -e

joinAD() {
    echo "joining gfoidl.at ..."
    net ads join -S addomain -U $adUser%$adPwd
    /etc/init.d/winbind restart
    
    echo
    echo "joined gfoidl.at"
}

joinAD

exec "$@"