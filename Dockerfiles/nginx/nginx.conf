# Entries in hosts-file have to be added! (Or directly in Fiddler when acting as proxy for the system)
# gfoidl.at
# auth.gfoidl.at

user                    nginx;
worker_processes        1;
pid                     /var/run/nginx.pid;

error_log               /var/log/nginx/error.log    warn;

events {
    worker_connections  1024;
}

http {
    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    keepalive_timeout   65;
    sendfile            on;

    server_tokens       off;                        # Hide version information in Server-header

    log_format          debug                       '$host $upstream_addr $remote_addr - $remote_user [$time_local] '
                                                    '$status "$request" $body_bytes_sent '
                                                    '"$http_referer" "$http_user_agent"';

    access_log          /var/log/nginx/access.log   debug;

    # Settings can be "global" (i.e. in http), per server or per location
    proxy_read_timeout  90;
    proxy_http_version  1.1;
    proxy_set_header    Connection keep-alive;
    proxy_cache_bypass  $http_upgrade;
    proxy_set_header    Upgrade                     $http_upgrade;
    proxy_set_header    Host                        $host;
    proxy_set_header    X-Real-IP                   $remote_addr;
    proxy_set_header    X-Forwarded-For             $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto           $scheme;

    proxy_redirect      ~*^(http://token-service)   https://auth.gfoidl.at;

    # Debug-Header
    add_header          X-Host                      $host;
    add_header          X-Backend-Server            $upstream_addr;

    ssl_certificate     /etc/nginx/certs/ssl.crt;
    ssl_certificate_key /etc/nginx/certs/ssl.key;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;
    ssl_session_cache   shared:SSL:10m;
    ssl_session_timeout 10m;
    
    gzip                on;                         # text/html is always compressed when on is set
    gzip_types          *;                          # all mime-types
    
    upstream website {
        server          website1;
        server          website2;
    }

    server {
        listen          80;
        server_name     gfoidl.at;
        server_name     www.gfoidl.at;

        # For testing 302 (temporary) --> in production 301 (permanent)
        return          302                         https://$host$request_uri;
    }

    server {
        listen          443 default_server          ssl;
        server_name     gfoidl.at;
        server_name     www.gfoidl.at;

        location / {
            proxy_pass  http://website;
        }
        
        location /favicon.ico {
            proxy_pass  http://nginx-website-static;
        }
        
        location /css/ {
            proxy_pass  http://nginx-website-static;
        }
        
        location /js/ {
            proxy_pass  http://nginx-website-static;
        }
        
        location /lib/ {
            proxy_pass  http://nginx-website-static;
        }
    }

    server {
        listen          80;
        server_name     auth.gfoidl.at;

        # For testing 302 (temporary) --> in production 301 (permanent)
        return          302                         https://$host$request_uri;
    }

    server {
        listen          443                         ssl;
        server_name     auth.gfoidl.at;

        location / {
            proxy_pass  http://token-service;

            # Cookie domain is set to the audience of the JWT in the token-service --> no need to manipulate
            # proxy_cookie_domain $host             gfoidl.at;
            # proxy_cookie_domain ~.*$              gfoidl.at;
        }
    }
}
