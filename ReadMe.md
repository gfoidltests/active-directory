# Username-format

Assume  

* domain _gfoidl.at_
* a user named _himen_

then for authentification possible values are:

* with GSS-API: _himen_  
* simple authentification:  
    * Administrator@gfoidl  
    * Administrator@gfoidl.at  
    * gfoidl\Administrator  
    * CN=Administrator,CN=Users,DC=gfoidl,DC=at  

# Usermanagement

* when a user is disabled, no login is possible
