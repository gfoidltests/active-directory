#!/bin/bash

run() {
    docker run --rm "$mode" \
        --network ad-network \
        -h redis \
        --name redis \
        redis:3.0-alpine "$@"
}

mode=

if [[ "$1" == "-i" ]]; then
    shift
    mode="-it"
elif [[ "$1" == "down" ]]; then
    docker stop redis
    exit 0
else
    mode="-d"
fi

run "$@"
