#!/bin/bash

run() {
    name=$1
    shift

    docker run --rm "$mode" \
        --network ad-network \
        -h "$name" \
        --name "$name" \
        -v "$(pwd)"/logs/"$name":/var/log/website \
        test/website "$@"
}

down() {
    docker stop "$1"
}

mode=

if [[ "$1" == "-i" ]]; then
    shift
    mode="-it"
elif [[ "$1" == "stop" ]]; then
    down website1
    down website2
    exit 0
else
    mode="-d"
fi

returnUrl=http://token-service

run website1 "$returnUrl" "$@"
run website2 "$returnUrl" "$@"
