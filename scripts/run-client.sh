#!/bin/bash

run() {
    docker run \
        --rm -it \
        -e adUser=$1 \
        -e adPwd=$2 \
        --network ad-network \
        -h $3 \
        --name $3 \
        test/sambaclient
}

showHelp() {
    echo usage: run-client.sh adUser adPwd name
}

if [[ $# != 3 ]]; then
    showHelp
else
    run $@
fi
