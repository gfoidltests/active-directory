#!/bin/bash

# cf. https://hub.docker.com/r/babim/sambaad/

# Instead of exposing individual ports one could use --network=host on docker run, to attach
# the container to the host's network.

runSetup() {
    echo starting samba in setup environment...

    docker run --rm -it \
        -e SAMBA_REALM="gfoidl.at" \
        -e SAMBA_DOMAIN="gfoidl" \
        -e SAMBA_PASSWORD="Password1!" \
        -e KERBEROS_PASSWORD="Password1!" \
        -e SAMBA_HOST_IP="127.0.0.1" \
        -e HOSTNAME="addomain" \
        -e SAMBA_SETUP_ONLY="true" \
        -e LDAP_ALLOW_INSECURE="true" \
        -v ${PWD}/samba-data/var/lib/samba:/var/lib/samba \
        -v ${PWD}/samba-data/etc/samba:/etc/samba \
        -v ${PWD}/samba-data/etc/openldap:/etc/openldap \
        -v ${PWD}/samba-data/var/lib/krb5kdc:/var/lib/krb5kdc \
        --name addomain \
        -h addomain \
        test/samba $@
}

runProduction() {
    echo starting samba in production environment...
    
    # cf. https://wiki.samba.org/index.php/Samba_AD_DC_Port_Usage

    docker run --rm -d \
        --privileged \
        -e SAMBA_REALM="gfoidl.at" \
        -e SAMBA_DOMAIN="gfoidl" \
        -e SAMBA_PASSWORD="Password1!" \
        -e SAMBA_HOST_IP="127.0.0.1" \
        -e HOSTNAME="addomain" \
        -e LDAP_ALLOW_INSECURE="true" \
        -v ${PWD}/samba-data/var/lib/samba:/var/lib/samba \
        -v ${PWD}/samba-data/etc/samba:/etc/samba \
        -v ${PWD}/samba-data/etc/openldap:/etc/openldap \
        -v ${PWD}/samba-data/var/lib/krb5kdc:/var/lib/krb5kdc \
        -p 53:53 -p 53:53/udp           `# DNS` \
        -p 88:88 -p 88:88/udp           `# Kerberos` \
        -p 135:135                      `# End Point Mapper` \
        -p 137:137/udp                  `# NetBIOS Name Service` \
        -p 138:138/udp                  `# NetBIOS Datagram` \
        -p 139:139                      `# NetBIOS Session` \
        -p 389:389 -p 389:389/udp       `# LDAP` \
        -p 636:636                      `# LDAP SSL ` \
        -p 445:445                      `# SMB over TCP` \
        -p 464:464 -p 464:464/udp       `# Kerberos kpasswd` \
        -p 3268:3268                    `# Global Catalog` \
        -p 3269:3269                    `# Global Catalog SSL` \
        --name addomain \
        --network ad-network \
        -h addomain \
        test/samba $@
}

runLDAP() {
    echo starting samba as LDAP...
    
    docker run --rm -d \
        --privileged \
        -e SAMBA_REALM="gfoidl.at" \
        -e SAMBA_DOMAIN="gfoidl" \
        -e SAMBA_PASSWORD="Password1!" \
        -e SAMBA_HOST_IP="127.0.0.1" \
        -e HOSTNAME="addomain" \
        -e LDAP_ALLOW_INSECURE="true" \
        -v ${PWD}/samba-data/var/lib/samba:/var/lib/samba \
        -v ${PWD}/samba-data/etc/samba:/etc/samba \
        -v ${PWD}/samba-data/etc/openldap:/etc/openldap \
        -v ${PWD}/samba-data/var/lib/krb5kdc:/var/lib/krb5kdc \
        -p 389:389 \
        -p 636:636 \
        --name addomain \
        --network ad-network \
        -h addomain \
        test/samba $@
}

showUsage() {
    echo "-s -> setup"
    echo "-p -> production"
    echo "-l -> ldap-server (same as production, but only tcp 389 opened)"
}

if [[ $# == "0" ]]; then
    showUsage
    exit 1
fi;

option=$1
shift

case $option in
    -s)
        runSetup $@
        ;;
    -p)
        runProduction $@
        ;;
    -l)
        runLDAP $@
        ;;
    *)
        showUsage
        ;;
esac
