#!/bin/bash

initLogs() {
    errorLog="$(pwd)/logs/nginx-website-static/error.log"
    accessLog="$(pwd)/logs/nginx-website-static/access.log"

    rm "$errorLog"
    rm "$accessLog"

    touch "$errorLog"
    touch "$accessLog"
}

run() {
    docker run --rm "$mode" \
        --network ad-network \
        -h nginx-website-static \
        --name nginx-website-static \
        -v "$(pwd)"/logs/nginx-website-static:/var/log/nginx \
        test/nginx-website-static "$@"
}

mode=

if [[ "$1" == "-i" ]]; then
    shift
    mode="-it"
elif [[ "$1" == "down" ]]; then
    docker stop nginx-website-static
    exit 0
else
    mode="-d"
fi

initLogs
run "$@"
