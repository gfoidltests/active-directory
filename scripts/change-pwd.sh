#/bin/bash

password=Aa..123456#
password='"'$password'"'
u16pass=$(printf $password | iconv -f ascii -t UTF16LE | base64)

echo "Debug -> $u16pass"

echo "dn: CN=himen,OU=Users,OU=test,DC=gfoidl,DC=at" > ldap.ldif
echo "changetype: modify" >> ldap.ldif
echo "replace: unicodePwd" >> ldap.ldif
echo "unicodePwd:: $u16pass" >> ldap.ldif 

echo
echo "Debug"
cat ldap.ldif
echo

ldapmodify -v -c -a -f ldap.ldif -H ldap://addomain -D Administrator@gfoidl.at -w Password1!
rm ldap.ldif
