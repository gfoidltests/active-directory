#!/bin/bash

# all 3 are equivalent, because bridge is default
# docker network create --driver bridge ad-network
# docker network create -d bridge ad-network
docker network create ad-network
