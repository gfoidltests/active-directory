#!/bin/bash

initLogs() {
    errorLog="$(pwd)/logs/nginx/error.log"
    accessLog="$(pwd)/logs/nginx/access.log"

    rm "$errorLog"
    rm "$accessLog"

    touch "$errorLog"
    touch "$accessLog"
}

run() {
    docker run --rm "$mode" \
        -p 80:80 \
        -p 443:443 \
        --network ad-network \
        -h nginx \
        --name nginx \
        -v "$(pwd)"/logs/nginx:/var/log/nginx \
        test/nginx "$@"
}

mode=

if [[ "$1" == "-i" ]]; then
    shift
    mode="-it"
elif [[ "$1" == "down" ]]; then
    docker stop nginx
    exit 0
else
    mode="-d"
fi

initLogs
run "$@"
