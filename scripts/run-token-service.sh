#!/bin/bash
    
run() {
    docker run --rm "$mode" \
        --network ad-network \
        -h token-service \
        --name token-service \
        -v "$(pwd)"/logs/token-service:/var/log/token-service \
        test/token-service "$@"
}

mode=

if [[ "$1" == "-i" ]]; then
    shift
    mode="-it"
elif [[ "$1" == "stop" ]]; then
    docker stop token-service
    exit 0
elif [[ "$1" == "--dev" ]]; then
    run addomain --dev "$@"
else
    mode="-d"
fi

run addomain "$@"
