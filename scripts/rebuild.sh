#!/bin/bash

set -e

showHelp() {
    echo rebuilds an image
    echo first argument is the image-name without the prefix 'test'
    echo second, and optional argument '-b' only builds the image, but does not start the container
}

if [[ $# -lt 1 ]]; then
    showHelp
    exit 1
fi

name=$1
shift 

./run-"$name".sh stop
docker rmi test/"$name"
# subshell so no cd - is needed
(
    cd Dockerfiles/"$name"
    docker build -t test/"$name" .
)

if [[ "$1" != "-b" ]]; then
    ./run-"$name".sh
fi
