#!/bin/bash

docker run --rm -d \
    -e PHPLDAPADMIN_LDAP_HOSTS=addomain \
    -e PHPLDAPADMIN_HTTPS=false \
    -p 8080:80 \
    --network ad-network \
    -h ad-admin \
    --name ad-admin \
    osixia/phpldapadmin
