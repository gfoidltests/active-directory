﻿using System;
using Moq;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests
{
    [TestFixture]
    public class VerifyPassword : Base
    {
        [Test]
        public void Credentials_wrong___false()
        {
            _ldapMock
                .Setup(l => l.Bind(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<Exception>()
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.VerifyPassword("userName", "password");

            Assert.IsFalse(actual);

            _ldapMock.Verify();
        }
        //---------------------------------------------------------------------
        [Test]
        public void Credentials_OK___true()
        {
            _ldapMock
                .Setup(l => l.Bind(It.IsAny<string>(), It.IsAny<string>()))
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.VerifyPassword("userName", "password");

            Assert.IsTrue(actual);

            _ldapMock.Verify();
        }
    }
}