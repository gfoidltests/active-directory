﻿using Moq;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests
{
    public abstract class Base
    {
        protected Mock<ILdapAdapter> _ldapMock;

        protected ILdapAdapter _ldap;
        //---------------------------------------------------------------------
        [SetUp]
        public void SetUp()
        {
            _ldapMock = new Mock<ILdapAdapter>();

            _ldap = _ldapMock.Object;
        }
        //---------------------------------------------------------------------
        protected ActiveDirectory CreateSut() => new ActiveDirectory(ldapAdapter: _ldap);
    }
}