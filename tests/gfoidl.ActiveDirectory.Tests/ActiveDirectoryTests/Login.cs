﻿using System;
using Moq;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests
{
    [TestFixture]
    public class Login : Base
    {
        [Test]
        public void User_is_enabled_Credentials_wrong___Authenticated_is_false()
        {
            _ldapMock
                .Setup(l => l.GetValue("userName", "userAccountControl"))
                .Returns("512")
                .Verifiable();

            _ldapMock
                .Setup(l => l.Bind("userName", "password"))
                .Throws<Exception>()
                .Verifiable();

            var sut = this.CreateSut();

            AdUser actual = sut.Login("userName", "password");

            Assert.AreEqual("userName", actual.UserName);
            Assert.IsTrue(actual.IsUserValid);
            Assert.IsFalse(actual.Authenticated);

            _ldapMock.Verify();
        }
        //---------------------------------------------------------------------
        [Test]
        public void User_is_enabled_Credentials_OK___Authenticated_is_true()
        {
            string[] groups = { "a", "b" };

            _ldapMock
                .Setup(l => l.GetValue("userName", "userAccountControl"))
                .Returns("512")
                .Verifiable();

            _ldapMock
                .Setup(l => l.Bind("userName", "password"))
                .Verifiable();

            _ldapMock
                .Setup(l => l.GetValue("userName", "displayName"))
                .Returns("DisplayName")
                .Verifiable();

            _ldapMock
                .Setup(l => l.GetGroupsForUser("userName"))
                .Returns(groups)
                .Verifiable();

            var sut = this.CreateSut();

            AdUser actual = sut.Login("userName", "password");

            Assert.AreEqual("userName", actual.UserName);
            Assert.IsTrue(actual.IsUserValid);
            Assert.IsTrue(actual.Authenticated);
#if DEBUG
            Assert.AreEqual("DisplayName DEBUG", actual.DisplayName);
#else
            Assert.AreEqual("DisplayName", actual.DisplayName);
#endif
            CollectionAssert.AreEquivalent(groups, actual.Groups);

            _ldapMock.Verify();
        }
        //---------------------------------------------------------------------
        [Test]
        public void User_is_disabled___Authenticated_is_false_and_VerifyPassword_not_called()
        {
            _ldapMock
                .Setup(l => l.GetValue("userName", "userAccountControl"))
                .Returns("514")
                .Verifiable();

            var sut = this.CreateSut();

            AdUser actual = sut.Login("userName", "password");

            Assert.AreEqual("userName", actual.UserName);
            Assert.IsFalse(actual.IsUserValid);
            Assert.IsFalse(actual.Authenticated);

            _ldapMock.Verify();
            _ldapMock.Verify(l => l.Bind("userName", "password"), Times.Never());
        }
    }
}