﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests
{
    [TestFixture]
    public class ChangePassword : Base
    {
        [Test]
        public void Credentials_wrong___false()
        {
            _ldapMock
                .Setup(l => l.Bind(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<Exception>()
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.ChangePassword("username", "password", "newPassword");

            _ldapMock.Verify();
            _ldapMock.Verify(l => l.ChangePassword(It.IsAny<string>(), It.IsAny<string>()), Times.Never());

            Assert.IsFalse(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Credentials_OK___true()
        {
            _ldapMock
                .Setup(l => l.Bind("userName", "password"))
                .Verifiable();

            _ldapMock
                .Setup(l => l.ChangePassword("userName", "newPassword"))
                .Returns(true)
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.ChangePassword("userName", "password", "newPassword");

            _ldapMock.Verify();

            Assert.IsTrue(actual);
        }
    }
}