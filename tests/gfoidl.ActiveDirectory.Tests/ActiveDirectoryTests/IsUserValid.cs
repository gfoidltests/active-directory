﻿using Moq;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests
{
    /*
     * For values see
     * https://social.technet.microsoft.com/Forums/windowsserver/en-US/7ff0fb2f-0cd1-44a9-b172-7abd196ee617/account-disabled-attribute-question?forum=winserverDS
     * http://www.selfadsi.de/ads-attributes/user-userAccountControl.htm
     */
    [TestFixture]
    public class IsUserValid : Base
    {
        [Test]
        public void User_is_disabled___false([Values("514", "66050")]string userAccountControl)
        {
            _ldapMock
                .Setup(l => l.GetValue(It.IsAny<string>(), "userAccountControl"))
                .Returns(userAccountControl)
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.IsUserValid("userName");

            Assert.IsFalse(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void User_is_enabled___true([Values("512", "66048")]string userAccountControl)
        {
            _ldapMock
                .Setup(l => l.GetValue(It.IsAny<string>(), "userAccountControl"))
                .Returns(userAccountControl)
                .Verifiable();

            var sut = this.CreateSut();

            bool actual = sut.IsUserValid("userName");

            Assert.IsTrue(actual);
        }
    }
}