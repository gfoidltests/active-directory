﻿using Novell.Directory.Ldap;
using NUnit.Framework;

namespace gfoidl.ActiveDirectory.Tests.ActiveDirectoryTests.Integration
{
    [TestFixture]
    public class ChangePassword
    {
        [Test, Explicit("Test-LDAP server has to run, and initial password set")]
        public void User_and_new_password_given___OK_and_password_changed()
        {
            string userName    = "himen@gfoidl.at";
            string oldPassword = "123456Aa...";
            string newPassword = "Aa..123456#";

            var sut = new ActiveDirectory();

            try
            {
                Assert.IsTrue (sut.VerifyPassword(userName, oldPassword), "pre | login with old pwd");
                Assert.IsFalse(sut.VerifyPassword(userName, newPassword), "pre | login with new pwd");

                Assert.IsTrue(sut.ChangePassword(userName, oldPassword, newPassword), "change pwd");

                Assert.IsFalse(sut.VerifyPassword(userName, oldPassword), "post | login with old pwd");
                Assert.IsTrue (sut.VerifyPassword(userName, newPassword), "post | login with new pwd");
            }
            catch (LdapException ex)
            {
                TestContext.WriteLine(ex.LdapErrorMessage);
                throw;
            }

            TestContext.WriteLine("password changed...resetting password to old value");
            sut.ChangePassword(userName, newPassword, oldPassword);
            TestContext.WriteLine("password set to old value");
        }
    }
}