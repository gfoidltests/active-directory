﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace token_service.Tests.UriExtensionsTests
{
    [TestFixture]
    public class GetCookieDomain
    {
        [Test, TestCaseSource("Uri_given___Correct_Domain_returned_TestCases")]
        public string Uri_given___Correct_Domain_returned(string uri)
        {
            return uri.GetCookieDomain();
        }
        //---------------------------------------------------------------------
        private static IEnumerable<TestCaseData> Uri_given___Correct_Domain_returned_TestCases()
        {
            yield return new TestCaseData("https://auth.gfoidl.at").Returns(".gfoidl.at");
            yield return new TestCaseData("http://auth.gfoidl.at").Returns(".gfoidl.at");
            yield return new TestCaseData("auth.gfoidl.at").Returns(".gfoidl.at");
            yield return new TestCaseData(".gfoidl.at").Returns(".gfoidl.at");
        }
    }
}