﻿using gfoidl.ActiveDirectory;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using token_service.Contracts;
using token_service.Controllers;

namespace token_service.Tests.Controllers.AuthControllerTests
{
    public abstract class Base
    {
        protected Mock<IActiveDirectory>         _adMock;
        protected Mock<ITokenService>            _tokenServiceMock;
        protected Mock<IOptions<AuthSettings>>   _optionsMock;
        protected Mock<ILogger<AuthController>>  _loggerMock;
        protected Mock<IHostingEnvironment>      _hostingMock;
        protected Mock<IRequestCookieCollection> _requestCookiesMock;
        protected Mock<IResponseCookies>         _responseCookiesMock;

        protected IActiveDirectory         _ad;
        protected ITokenService            _tokenService;
        protected IOptions<AuthSettings>   _options;
        protected ILogger<AuthController>  _logger;
        protected IHostingEnvironment      _hosting;
        protected IRequestCookieCollection _requestCookies;
        protected IResponseCookies         _responseCookies;
        protected AuthSettings             _authSettings;
        //---------------------------------------------------------------------
        [SetUp]
        public void SetUp()
        {
            _adMock              = new Mock<IActiveDirectory>();
            _tokenServiceMock    = new Mock<ITokenService>();
            _optionsMock         = new Mock<IOptions<AuthSettings>>();
            _loggerMock          = new Mock<ILogger<AuthController>>();
            _hostingMock         = new Mock<IHostingEnvironment>();
            _requestCookiesMock  = new Mock<IRequestCookieCollection>();
            _responseCookiesMock = new Mock<IResponseCookies>();

            _ad              = _adMock.Object;
            _tokenService    = _tokenServiceMock.Object;
            _options         = _optionsMock.Object;
            _logger          = _loggerMock.Object;
            _hosting         = _hostingMock.Object;
            _requestCookies  = _requestCookiesMock.Object;
            _responseCookies = _responseCookiesMock.Object;

            _authSettings = new AuthSettings
            {
                Audience            = "http://test.at",
                CertKeyPath         = "keyPath",
                ExpirationInMinutes = 5,
                Issuer              = "http://test.at",
                CookieName          = "access_token",
                ReturnUrlCookieName = "returnUrl"
            };

            _optionsMock
                .Setup(o => o.Value)
                .Returns(_authSettings);
        }
        //---------------------------------------------------------------------
        protected AuthController CreateSut()
        {
            var sut = new AuthController(_ad, _tokenService, _options, _logger, _hosting);

            var request = new Mock<HttpRequest>();
            request
                .Setup(r => r.Cookies)
                .Returns(_requestCookies);

            var response = new Mock<HttpResponse>();
            response
                .Setup(r => r.Headers)
                .Returns(new HeaderDictionary());

            response
                .Setup(r => r.Cookies)
                .Returns(_responseCookies);

            var httpContext = new Mock<HttpContext>();

            httpContext
                .Setup(c => c.Request)
                .Returns(request.Object);

            httpContext
                .Setup(c => c.Response)
                .Returns(response.Object);

            sut.ControllerContext = new ControllerContext
            {
                HttpContext = httpContext.Object
            };

            return sut;
        }
    }
}