﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using gfoidl.ActiveDirectory;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;

namespace token_service.Tests.Controllers.AuthControllerTests
{
    [TestFixture]
    public class Token : Base
    {
        [Test]
        public async Task Http_Get_Credentials_OK___Token_returned()
        {
            AdUser adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = true
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var principal          = new ClaimsPrincipal();
            var token              = new JwtSecurityToken();
            string serializedToken = "serializedToken";

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(adUser))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, true))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = await sut.Token("userName", "password");
            var okResult         = actual as OkObjectResult;

            Assert.AreEqual(serializedToken, okResult.Value);

            _adMock.Verify();
            _tokenServiceMock.Verify();
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Get_Unauthorized___Http_401()
        {
            AdUser adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = false
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = await sut.Token("userName", "password");

            Assert.IsInstanceOf<UnauthorizedResult>(actual);

            _adMock.Verify();
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Get_Exception___Http_400()
        {
            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Throws<Exception>()
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = await sut.Token("userName", "password");

            Assert.IsInstanceOf<BadRequestResult>(actual);

            _adMock.Verify();
        }
    }
}