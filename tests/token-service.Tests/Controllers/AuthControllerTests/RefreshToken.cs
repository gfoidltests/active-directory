﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace token_service.Tests.Controllers.AuthControllerTests
{
    [TestFixture]
    public class RefreshToken : Base
    {
        [Test]
        public void Token_given___correct_Methods_called()
        {
            string oldToken     = "oldToken";
            var principal       = new ClaimsPrincipal();
            var token           = new JwtSecurityToken();
            var serializedToken = "serializedToken";

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(oldToken))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, false))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = sut.RefreshToken(oldToken);
            var okResult         = actual as OkObjectResult;

            _tokenServiceMock.Verify();

            Assert.AreEqual(serializedToken, okResult.Value);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Token_given_and_setCookie___Cookie_set()
        {
            string oldToken             = "oldToken";
            var principal               = new ClaimsPrincipal();
            var token                   = new JwtSecurityToken();
            var serializedToken         = "serializedToken";
            CookieOptions cookieOptions = null;

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(oldToken))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, false))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            _responseCookiesMock
                .Setup(c => c.Append("access_token", "serializedToken", It.IsAny<CookieOptions>()))
                .Callback<string, string, CookieOptions>((key, value, opts) => cookieOptions = opts)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = sut.RefreshToken(oldToken, true);
            var okResult         = actual as OkObjectResult;

            _tokenServiceMock.Verify();
            _responseCookiesMock.Verify();

            Assert.IsTrue(cookieOptions.HttpOnly);
            Assert.IsTrue(cookieOptions.Expires.HasValue);
            Assert.AreEqual(new Uri(_authSettings.Audience).Host, cookieOptions.Domain);
        }
    }
}