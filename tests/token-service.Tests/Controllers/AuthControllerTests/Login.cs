﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using gfoidl.ActiveDirectory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace token_service.Tests.Controllers.AuthControllerTests
{
    [TestFixture]
    public class Login : Base
    {
        [Test]
        public async Task Http_Post_Credentials_OK___Redirect_to_returnUrl()
        {
            var adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = true
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var principal       = new ClaimsPrincipal();
            var token           = new JwtSecurityToken();
            var serializedToken = "serializedToken";
            string returnUrl = $"http://foo?q={Guid.NewGuid()}";

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(adUser))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, true))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            _requestCookiesMock
                .Setup(c => c["returnUrl"])
                .Returns(returnUrl);

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var redirectResult   = actual as RedirectResult;

            _adMock.Verify();
            _tokenServiceMock.Verify();

            Assert.AreEqual(returnUrl, redirectResult.Url);
            Assert.IsFalse(redirectResult.Permanent);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_Credentials_OK___Redirect_cookie_deleted()
        {
            var adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = true
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var principal       = new ClaimsPrincipal();
            var token           = new JwtSecurityToken();
            var serializedToken = "serializedToken";
            string returnUrl    = $"http://foo?q={Guid.NewGuid()}";

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(adUser))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, true))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            _requestCookiesMock
                .Setup(c => c["returnUrl"])
                .Returns(returnUrl);

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var redirectResult   = actual as RedirectResult;

            _responseCookiesMock.Verify(c => c.Delete(_authSettings.ReturnUrlCookieName, It.IsAny<CookieOptions>()), Times.Once());
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_Credentials_OK___Cookie_set()
        {
            var adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = true
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var principal               = new ClaimsPrincipal();
            var token                   = new JwtSecurityToken();
            var serializedToken         = "serializedToken";
            CookieOptions cookieOptions = null;

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(adUser))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, true))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            _responseCookiesMock
                .Setup(c => c.Append("access_token", "serializedToken", It.IsAny<CookieOptions>()))
                .Callback<string, string, CookieOptions>((key, value, opts) => cookieOptions = opts)
                .Verifiable();

            _requestCookiesMock
                .Setup(c => c["returnUrl"])
                .Returns("http://returnUrl.at");

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var redirectResult   = actual as RedirectResult;

            _responseCookiesMock.Verify();

            Assert.IsTrue(cookieOptions.HttpOnly);
            Assert.IsTrue(cookieOptions.Expires.HasValue);
            Assert.AreEqual(new Uri(_authSettings.Audience).Host, cookieOptions.Domain);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_Credentials_OK_Development_environment___CookieDomain_not_set()
        {
            var adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = true
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var principal               = new ClaimsPrincipal();
            var token                   = new JwtSecurityToken();
            var serializedToken         = "serializedToken";
            CookieOptions cookieOptions = null;

            _tokenServiceMock
                .Setup(t => t.CreateClaimsPrincipal(adUser))
                .Returns(principal)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.CreateJwtSecurityToken(principal, true))
                .Returns(token)
                .Verifiable();

            _tokenServiceMock
                .Setup(t => t.SerialiazeToken(token))
                .Returns(serializedToken)
                .Verifiable();

            _responseCookiesMock
                .Setup(c => c.Append("access_token", "serializedToken", It.IsAny<CookieOptions>()))
                .Callback<string, string, CookieOptions>((key, value, opts) => cookieOptions = opts)
                .Verifiable();

            _hostingMock
                .Setup(h => h.EnvironmentName)
                .Returns("Development")
                .Verifiable();

            _requestCookiesMock
                .Setup(c => c["returnUrl"])
                .Returns("http://returnUrl.at");

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var redirectResult   = actual as RedirectResult;

            _hostingMock.Verify();

            Assert.IsNull(cookieOptions.Domain);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_User_does_not_exist___Returns_Login_View()
        {
            AdUser adUser = new AdUser
            {
                IsUserValid   = false,
                Authenticated = false
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var viewResult       = actual as ViewResult;

            _adMock.Verify();

            Assert.AreEqual("Login", viewResult.ViewName);
            Assert.AreEqual("Username or password are incorrect", viewResult.ViewData["Message"]);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_Unauthorized___Returns_Login_View()
        {
            AdUser adUser = new AdUser
            {
                IsUserValid   = true,
                Authenticated = false
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual = await sut.Login("userName", "password");
            var viewResult       = actual as ViewResult;

            _adMock.Verify();

            Assert.AreEqual("Login", viewResult.ViewName);
            Assert.AreEqual("Username or password are incorrect", viewResult.ViewData["Message"]);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Http_Post_User_is_not_valid___Returns_UserNotValid_View()
        {
            AdUser adUser = new AdUser
            {
                DisplayName = "DisplayName for user",
                IsUserValid = false
            };

            _adMock
                .Setup(a => a.Login("userName", "password"))
                .Returns(adUser)
                .Verifiable();

            var sut = this.CreateSut();

            IActionResult actual       = await sut.Login("userName", "password");
            var redirectToActionResult = actual as RedirectToActionResult;

            _adMock.Verify();

            Assert.IsFalse(redirectToActionResult.Permanent);
            Assert.AreEqual("UserNotValid"  , redirectToActionResult.ActionName);
            Assert.AreEqual("userName"      , redirectToActionResult.RouteValues["userName"]);
        }
    }
}