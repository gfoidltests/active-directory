#!/bin/bash

set -e

makecert() {
    openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:4096 -keyout "$1".key -out "$1".crt -config openssl.cnf
    openssl pkcs12 -export -out "$1".pfx -inkey "$1".key -in "$1".crt
}

makecert token
makecert ssl

mv ssl.* ../Dockerfiles/nginx
